package HomeWork;
import java.io.BufferedReader;

import java.io.IOException;

import java.io.InputStreamReader;
import java.util.Arrays;
/**
 * Created by user on 01.11.2016.
 */
public class HomeWork {
    public static int minOfThreeN (int a, int b, int c) {
        if (a < b && a < c) return a;
        if (b < a && b < c) return b;
        if (c < b && c < a) return c;
        else return a;
    }




    public static int factorial1 (int a) {
        int fact = 1;

        int num = 0;

        while (num < a) {

            fact *= ++num;

        }

        return fact;

    }



    public static int factorial2 (int a) {

        int fact = 1;

        int num = 0;

        do {
            fact *= ++num;

        }
        while (num < a);

        return fact;

    }




    public static int factorial3 (int a) {

        int fact = 1;

        for (int k = 1; k <= a; k++) {

            fact *= k;

        }

        return fact;

    }


    public static String ArrayOne() {
        String result = "";
        StringBuilder sb = new StringBuilder();
        int[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        int[] b = Arrays.copyOf(a, 10);
        for (int i = 0; i < b.length; i++)
            sb.append(b[i]).append(" ");
        return sb.toString();
    }

    public static String ArrayExceptOfFive() {

        String result = "";

        StringBuilder sb = new StringBuilder();

        int[] array ={1, 3, 5, 2, 4, 5, 5, 6, 5, 8, 5};

        for (int i = 0; i < array.length; i++) {

            if (array[i] != 5) {

                sb.append(array[i]).append(" ");

            }

        }

        sb.deleteCharAt(sb.length() - 1);

        return sb.toString();

    }




    public static void Calculator () throws IOException {

        int a = 15;

        int b = 3;

        String result = "";

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String action = reader.readLine();

        switch (action) {

            case "+": result = String.format("addition is %d", a + b); break;

            case "-": result = String.format("subtraction is %d", a - b); break;

            case "*": result = String.format("multiplication is %d", a * b); break;

            case "/": result = b != 0 ? String.format("division is %d", a / b) : String.format("Error: / by zero"); break;

            default: result = String.format("Unsupported operation: %s", action);

        }

        System.out.println(result);

    }



    public static void printMinAndMaxOfArray() {

        int[] array1dim = {0, 1, 4, 3, 7, 5, -1, -2, -3, -5};

        int min = array1dim[0];

        int max = array1dim[0];

        for (int i = 0; i < array1dim.length; i++) {

            if (array1dim[i] < min) min = array1dim[i];

            if (array1dim[i] > max) max = array1dim[i];

        }

        System.out.printf("Min element of one-dimension array : %d, max element: %d%n", min, max);


        int[][] array2dim = {   {0, 1, 4, 3, 7, 5, -1, -2, -3, -5},

                {10, 7, 1, 30, -11, -8},

                {31, 6, 13}
        };

        min = array2dim[0][0];

        max = array2dim[0][0];

        for (int i = 0; i < array2dim.length; i++) {

            for (int j = 0; j < array2dim[i].length; j++) {

                if (array2dim[i][j] < min) min = array2dim[i][j];

                if (array2dim[i][j] > max) max = array2dim[i][j];

            }

        }

        System.out.printf("Min element of two-dimension array : %d, max element: %d%n", min, max);

    }




    public static void  printSumElementsBelowMainDiagonalTwoDimensionArray(int size) {



        int[][] array = new int[size][size];

        for (int i = 0; i < array.length; i++) {

            for (int j = 0; j < array[i].length; j++) {

                array[i][j] = (int)(Math.random()*30 - Math.random()*30);

                System.out.print(array[i][j] + "\t");

            }

            System.out.println();

        }


        int sum = 0;

        for (int i = 1; i < array.length; i++) {

            for (int j = 0; j < i; j++) {

                sum += array[i][j];

            }

        }

        System.out.printf("Sum of all elements below the diagonal in a two-dimensional array: %d%n", sum);

    }



    public static void AllFridaysInMoth() {
                        /*Monday = 1-stDay;
                        Start with Sunday = 0;*/

        int[] month = new int[31];


        String[] WeekDays = {"Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};


        for (int i = 0; i < month.length; i++) {

            month[i] = i + 1;

        }

        for (int i = 4; i < month.length; i += 7) {

            System.out.printf("%d is %s!%n", month[i], WeekDays[5]);

        }
    }

    public static void Day31th() {
        int[] month = new int[31];

        String[] WeekDays = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",};


        String dayOf31th = WeekDays[31 % 7];

        System.out.printf("31 is %s!%n", dayOf31th);


    }





    public static void main(String[] args) throws IOException {

        System.out.printf("A minimum of three numbers: %d%n", minOfThreeN(56, 10, 18));

        System.out.printf("Calculation of the factorial of %d \"while\" statement: %d%n", 67, factorial1(67));

        System.out.printf("Calculation of the factorial of %d \"do-while\" statement: %d%n", 67, factorial2(67));

        System.out.printf("Calculation of the factorial of %d  \"for\" statement: %d%n", 67, factorial3(67));

        System.out.printf("Printing the array elements: %s%n", ArrayOne());

        System.out.printf("Printing the array elements without \"5\" element: %s%n", ArrayExceptOfFive());

        System.out.println("Calculator. Enter operation from list [+ - * /]");

        Calculator();

        System.out.println("Minimum and maximum one-dimensional array and a two-dimensional array:");

        printMinAndMaxOfArray();

        System.out.println("Sum of all elements below the diagonal in a two-dimensional array:");

        printSumElementsBelowMainDiagonalTwoDimensionArray(5);

        System.out.println("\n" + "Find all the Fridays of this month:");

        AllFridaysInMoth();

        System.out.println("\n" + "Determine the day of the week 31 th.:");
        Day31th();

    }

}









